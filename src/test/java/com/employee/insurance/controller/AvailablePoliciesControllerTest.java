package com.employee.insurance.controller;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.employee.insurance.entity.AvailablePolicies;
import com.employee.insurance.entity.EmployeePolicies;
import com.employee.insurance.service.AvailablePoliciesService;
import com.employee.insurance.service.EmployeePoliciesService;

@SpringBootTest
public class AvailablePoliciesControllerTest {

	@Mock
	private PolicyController availablePoliciesController;
	
	@InjectMocks
	AvailablePoliciesService availablePoliciesService;
	
	@InjectMocks
	EmployeePoliciesService employeePoliciesService;
	
	@org.junit.Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testgetPolicies() {
		List<AvailablePolicies> availablePoliciesResponseList = new ArrayList<AvailablePolicies>();
		AvailablePolicies policy = new AvailablePolicies();
		policy.setPolicyId(1L);
		policy.setActiveStatus(true);;
		List<EmployeePolicies> employeePolicies = new ArrayList<EmployeePolicies>();
		//policy.setEmployeePolicies(employeePolicies );
		availablePoliciesResponseList.add(policy);
		try {
			when(availablePoliciesService.getPolicies()).thenReturn(availablePoliciesResponseList);
			availablePoliciesController.getPolicies();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testgetPoliciesById() {
		AvailablePolicies policy = new AvailablePolicies();
		policy.setPolicyId(1L);
		try {
			doReturn(policy).when(availablePoliciesService).getPoliciesById(1L);
			availablePoliciesController.getPoliciesById(1L);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testgetPoliciesSalientFeatursById() {
		AvailablePolicies policy = new AvailablePolicies();
		policy.setPolicyId(1L);
		try {
			doReturn(policy).when(availablePoliciesService).getPoliciesById(1L);
			availablePoliciesController.getPoliciesSalientFeatursById(1L);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
