package com.employee.insurance.controller;

import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.employee.insurance.dto.EmployeePoliciesRequestDto;
import com.employee.insurance.dto.EmployeePoliciesResponseDto;
import com.employee.insurance.entity.EmployeePolicies;
import com.employee.insurance.service.EmployeePoliciesService;

@SpringBootTest
public class EmployeePoliciesControllerTest {

	@InjectMocks
	private EmployeePolicyController employeePoliciesController;
	
	@Mock
	private EmployeePoliciesService employeePoliciesService;
	
	@org.junit.Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testGetPolicyHolderByCounts() {
		List<EmployeePolicies> employeePolicies = new ArrayList<>();
		EmployeePolicies policies = new EmployeePolicies();
		policies.setEmpPolicyId(1L);
		employeePolicies.add(policies);
		try {
			doReturn(employeePolicies).when(employeePoliciesService).getPolicyHolderByCounts();
			employeePoliciesController.getPoliciesByCounts();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testenrollPolicy() {
		EmployeePoliciesResponseDto response = new EmployeePoliciesResponseDto();
		EmployeePoliciesRequestDto requestDto = new EmployeePoliciesRequestDto();
		requestDto.setEmpId(1L);
		requestDto.setPolicyId(1L);
		//requestDto.setPolicyNum("12345");
		requestDto.setPremiumAmount(123.123);
		doReturn(response).when(employeePoliciesService).enrollPolicy(requestDto);
		employeePoliciesController.enrollPolicy(requestDto );
	}
}
