package com.employee.insurance.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "AVAILABE_POLICIES")
public class AvailablePolicies extends AuditModel {

	private static final long serialVersionUID = 1L;

	@Id
	@NotNull(message = "policyId is required")
	@Column(name = "policy_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long policyId;
	@Column(name = "policy_name")
	@NotEmpty(message = "policy name is required")
	private String policyName;
	@Column(name = "policy_desc")
	@NotNull(message = "employee policy desc is required")
	private String policyDesc;
	@Column(name = "entry_age")
	@NotNull(message = "entryAge is required")
	private Long entryAge;

	@NotNull(message = "maximum maturity age is required")
	@Column(name = "maximum_maturity_age")
	private Long maximumMaturityAge;
	@Column(name = "policy_term")
	@NotEmpty(message = "policy term is required")
	private String policyTerm;
	@Column(name = "minimum_premium")
	@NotNull(message = "minimum premium is required")
	private double minimumPremium;
	@Column(name = "minimum_sum_assured")
	@NotNull(message = "minimum_sum_assured is required")
	private double minimumSumAssured;
	@Column(name = "active_status")
	@NotNull(message = "active status is required")
	private boolean activeStatus;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "availablePolicies")
	@JsonIgnore
	private List<SalientFeature> salientFeatures;
	
	@NotNull(message = "termAndCondition is required")
	@Column(name = "term_and_condition")
	private String termAndCondition;

	public AvailablePolicies() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AvailablePolicies(@NotNull(message = "policyId is required") Long policyId,
			@NotEmpty(message = "policy name is required") String policyName,
			@NotNull(message = "employee policy desc is required") String policyDesc,
			@NotNull(message = "entryAge is required") Long entryAge,
			@NotNull(message = "maximum maturity age is required") Long maximumMaturityAge,
			@NotEmpty(message = "policy term is required") String policyTerm,
			@NotNull(message = "minimum premium is required") double minimumPremium,
			@NotNull(message = "minimum_sum_assured is required") double minimumSumAssured,
			@NotNull(message = "active status is required") boolean activeStatus, List<SalientFeature> salientFeatures,
			@NotNull(message = "termAndCondition is required") String termAndCondition) {
		super();
		this.policyId = policyId;
		this.policyName = policyName;
		this.policyDesc = policyDesc;
		this.entryAge = entryAge;
		this.maximumMaturityAge = maximumMaturityAge;
		this.policyTerm = policyTerm;
		this.minimumPremium = minimumPremium;
		this.minimumSumAssured = minimumSumAssured;
		this.activeStatus = activeStatus;
		this.salientFeatures = salientFeatures;
		this.termAndCondition = termAndCondition;
	}

	public String getTermAndCondition() {
		return termAndCondition;
	}

	public void setTermAndCondition(String termAndCondition) {
		this.termAndCondition = termAndCondition;
	}

	public double getMinimumSumAssured() {
		return minimumSumAssured;
	}

	public void setMinimumSumAssured(double minimumSumAssured) {
		this.minimumSumAssured = minimumSumAssured;
	}

	public Long getPolicyId() {
		return policyId;
	}

	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}

	public String getPolicyName() {
		return policyName;
	}

	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}

	public String getPolicyDesc() {
		return policyDesc;
	}

	public void setPolicyDesc(String policyDesc) {
		this.policyDesc = policyDesc;
	}

	public Long getEntryAge() {
		return entryAge;
	}

	public void setEntryAge(Long entryAge) {
		this.entryAge = entryAge;
	}

	public Long getMaximumMaturityAge() {
		return maximumMaturityAge;
	}

	public void setMaximumMaturityAge(Long maximumMaturityAge) {
		this.maximumMaturityAge = maximumMaturityAge;
	}

	public String getPolicyTerm() {
		return policyTerm;
	}

	public void setPolicyTerm(String policyTerm) {
		this.policyTerm = policyTerm;
	}

	public double getMinimumPremium() {
		return minimumPremium;
	}

	public void setMinimumPremium(double minimumPremium) {
		this.minimumPremium = minimumPremium;
	}

	public boolean isActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(boolean activeStatus) {
		this.activeStatus = activeStatus;
	}

	public List<SalientFeature> getSalientFeatures() {
		return salientFeatures;
	}

	public void setSalientFeatures(List<SalientFeature> salientFeatures) {
		this.salientFeatures = salientFeatures;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
