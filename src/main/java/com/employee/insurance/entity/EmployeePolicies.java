package com.employee.insurance.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Employee_Policies")
public class EmployeePolicies extends AuditModel {
	private static final long serialVersionUID = 1L;
	
	
	public EmployeePolicies(long empPolicyId,
			double premiumAmount,
			String policyNum, Employee employee,
			AvailablePolicies availablePolicies) {
		super();
		this.empPolicyId = empPolicyId;
		this.premiumAmount = premiumAmount;
		this.policyNum = policyNum;
		this.employee = employee;
		this.availablePolicies = availablePolicies;
	}

	@Id
	@NotNull(message = "Employye policy id is required")
	@Column(name = "emp_policy_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long empPolicyId;
	@NotNull(message = "premium amount is required")
	@Column(name = "premium_amount")
	private double premiumAmount;
	

	@NotNull(message = "policy_num is required")
	@Column(name = "policy_num")
	private String policyNum;

	public EmployeePolicies() {

	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "EMP_ID")
	@JsonIgnore
	private Employee employee;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "policy_id")
	private AvailablePolicies availablePolicies;

	public AvailablePolicies getAvailablePolicies() {
		return availablePolicies;
	}


	public void setAvailablePolicies(AvailablePolicies availablePolicies) {
		this.availablePolicies = availablePolicies;
	}


	public long getEmpPolicyId() {
		return empPolicyId;
	}

	public void setEmpPolicyId(long empPolicyId) {
		this.empPolicyId = empPolicyId;
	}

	public double getPremiumAmount() {
		return premiumAmount;
	}

	public void setPremiumAmount(double premiumAmount) {
		this.premiumAmount = premiumAmount;
	}


	public String getPolicyNum() {
		return policyNum;
	}

	public void setPolicyNum(String policyNum) {
		this.policyNum = policyNum;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}
