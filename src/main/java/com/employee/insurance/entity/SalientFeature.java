package com.employee.insurance.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Salient_Feature")
public class SalientFeature implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public SalientFeature() {

	}

	public SalientFeature(Long salientFeatureId, String policySalientFeature, AvailablePolicies availablePolicies) {
		super();
		this.salientFeatureId = salientFeatureId;
		this.policySalientFeature = policySalientFeature;
		this.availablePolicies = availablePolicies;
	}

	@Id
	@Column(name = "salient_feature_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long salientFeatureId;
	@Column(name = "policy_salient_Feature")
	private String policySalientFeature;

	@ManyToOne
	@JoinColumn(name = "policy_id")
	@JsonIgnore
	private AvailablePolicies availablePolicies;

	public Long getSalientFeatureId() {
		return salientFeatureId;
	}

	public void setSalientFeatureId(Long salientFeatureId) {
		this.salientFeatureId = salientFeatureId;
	}

	public String getPolicySalientFeature() {
		return policySalientFeature;
	}

	public void setPolicySalientFeature(String policySalientFeature) {
		this.policySalientFeature = policySalientFeature;
	}

	public AvailablePolicies getAvailablePolicies() {
		return availablePolicies;
	}

	public void setAvailablePolicies(AvailablePolicies availablePolicies) {
		this.availablePolicies = availablePolicies;
	}
}
