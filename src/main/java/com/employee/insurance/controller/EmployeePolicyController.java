package com.employee.insurance.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.insurance.dto.EmployeePoliciesRequestDto;
import com.employee.insurance.dto.EmployeePoliciesResponseDto;
import com.employee.insurance.dto.TrendResponseDto;
import com.employee.insurance.service.EmployeePoliciesService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/employeepolicy")
@Api("Insurance Policies by number of poilcy holders")
public class EmployeePolicyController {
	@Autowired
	EmployeePoliciesService employeePoliciesService;
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeePolicyController.class);

	@GetMapping
	@ApiOperation("trends")
	public ResponseEntity<List<TrendResponseDto>> getPoliciesByCounts() throws Exception {
		LOGGER.debug("AvailablePoliciesController :: All Policies :: start");
		List<TrendResponseDto> employeePolicies =  employeePoliciesService.getPolicyHolderByCounts();
		LOGGER.debug("AvailablePoliciesController :: All Policies :: end");
		return new ResponseEntity<>(employeePolicies, HttpStatus.OK);
	}

	@PostMapping("/enroll")
	@ApiOperation("enrolling for policy")
	public ResponseEntity<EmployeePoliciesResponseDto> enrollPolicy(@RequestBody EmployeePoliciesRequestDto requestDto) {
		EmployeePoliciesResponseDto response = employeePoliciesService.enrollPolicy(requestDto);
		return new ResponseEntity<>(response,HttpStatus.CREATED);
	}

}
