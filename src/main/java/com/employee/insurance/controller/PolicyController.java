package com.employee.insurance.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.employee.insurance.dto.AvailablePolicySalientFeatureResDto;
import com.employee.insurance.entity.AvailablePolicies;
import com.employee.insurance.exception.PolicyNotFoundException;
import com.employee.insurance.service.AvailablePoliciesService;
import com.employee.insurance.service.EmployeePoliciesService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/policies")
@Api("Operations Pertaining to Insurance Policies")
public class PolicyController {

	@Autowired
	AvailablePoliciesService availablePoliciesService;
	@Autowired
	EmployeePoliciesService employeePoliciesService;

	private static final Logger LOGGER = LoggerFactory.getLogger(PolicyController.class);

	@GetMapping
	@ApiOperation("Available insurance policies")
	@ResponseBody
	public List<AvailablePolicies> getPolicies() throws Exception {
		LOGGER.info("AvailablePoliciesController :: All Policies :: start");
		List<AvailablePolicies> availablePoliciesResponseList = new ArrayList<AvailablePolicies>();
		availablePoliciesResponseList = availablePoliciesService.getPolicies();
		LOGGER.info("AvailablePoliciesController :: All Policies :: end");
		return availablePoliciesResponseList;
	}

	@ApiOperation("Search insurance policies by policy id")
	@GetMapping("/{policyId}")
	public ResponseEntity<AvailablePolicies> getPoliciesById(@PathVariable("policyId") Long policyId)
			throws PolicyNotFoundException {
		LOGGER.info("****Inside available policy method by policy id ****");
		LOGGER.info("search() method called by policyId");
		AvailablePolicies availablePolicies = availablePoliciesService.getPoliciesById(policyId);
		return new ResponseEntity<>(availablePolicies, HttpStatus.OK);
	}

	@ApiOperation("Search insurance policies by policy id")
	@GetMapping("/{policyId}/salientFeatures")
	public ResponseEntity<AvailablePolicySalientFeatureResDto> getPoliciesSalientFeatursById(
			@PathVariable("policyId") Long policyId) throws PolicyNotFoundException {
		LOGGER.info("****Inside available policy method by policy id ****");
		LOGGER.info("search() method called by policyId");
		AvailablePolicies availablePolicies = availablePoliciesService.getPoliciesById(policyId);
		AvailablePolicySalientFeatureResDto availablePolicySalientFeatureResDto = new AvailablePolicySalientFeatureResDto();
		availablePolicySalientFeatureResDto.setSalientFeatures(availablePolicies.getSalientFeatures());
		return new ResponseEntity<>(availablePolicySalientFeatureResDto, HttpStatus.OK);
	}

	/*@ApiOperation("Search insurance policies by policy id")
	@GetMapping("/Temp/{policyId}")
	public ResponseEntity<AvailablePoliciesTermAndConditionDto> getPoliciesTermAndConditionById(
			@PathVariable("policyId") Long policyId)
			throws PolicyNotFoundException {
		LOGGER.info("****Inside available policy method by policy id ****");
		LOGGER.info("search() method called by policyId");
		AvailablePolicies availablePolicies=null;
		AvailablePoliciesTermAndConditionDto availablePoliciesTermAndConditionDto=null;
		availablePolicies = availablePoliciesService.getPoliciesById(policyId);
					availablePoliciesTermAndConditionDto = new AvailablePoliciesTermAndConditionDto();
					availablePoliciesTermAndConditionDto
							.setTermAndCondition(availablePolicies.getTermAndCondition());
		
		
		
		return new ResponseEntity<>(availablePoliciesTermAndConditionDto, HttpStatus.OK);
	}*/

}
