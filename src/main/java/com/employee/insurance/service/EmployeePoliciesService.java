package com.employee.insurance.service;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.insurance.constants.AppConstants;
import com.employee.insurance.dto.EmployeePoliciesRequestDto;
import com.employee.insurance.dto.EmployeePoliciesResponseDto;
import com.employee.insurance.dto.TrendResponseDto;
import com.employee.insurance.entity.AvailablePolicies;
import com.employee.insurance.entity.Employee;
import com.employee.insurance.entity.EmployeePolicies;
import com.employee.insurance.exception.PolicyNotFoundException;
import com.employee.insurance.repository.AvailablePoliciesRepository;
import com.employee.insurance.repository.EmployeePoliciesRepository;
import com.employee.insurance.repository.EmployeeRepository;

@Service
public class EmployeePoliciesService {
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeePoliciesService.class);

	@Autowired
	EmployeePoliciesRepository employeePoliciesRepository;
	@Autowired
	EmployeeRepository employeeRepository;
	@Autowired
	AvailablePoliciesRepository availablePoliciesRepository;


	public EmployeePolicies getPoliciesById(Long empPolicyId) throws PolicyNotFoundException {
		LOGGER.info("Inside getPoliciesById()");
		Optional<EmployeePolicies> employeePolicies = employeePoliciesRepository.findById(empPolicyId);
		if (employeePolicies.isPresent()) {
			return employeePolicies.get();
		} else {
			throw new PolicyNotFoundException("Policy Not Found");
		}
	}

	public List<TrendResponseDto> getPolicyHolderByCounts() throws PolicyNotFoundException {
		List<TrendResponseDto> ltrd = new LinkedList<TrendResponseDto>();
		List<?> employeePolicies = employeePoliciesRepository.getPolicyHolderByCounts();
		if (!Objects.isNull(employeePolicies)) {
			for (Iterator<?> iterator = employeePolicies.iterator(); iterator.hasNext();) {
				TrendResponseDto trd = new TrendResponseDto();
				Object[] object = (Object[]) iterator.next();
				String policyId = object[0].toString();
				if(null!=policyId) {
					Optional<AvailablePolicies> ap = availablePoliciesRepository.findById(Long.parseLong(policyId));
					if(ap.isPresent()) {
						trd.setPolicyName(ap.get().getPolicyName());
					}
				}
				if(null!=object[1].toString()) {
					trd.setNoOfPolicyHolder(object[1].toString());
				}
				if(null!=object[2].toString()) {
					trd.setPolicy_percentage(object[2].toString());
				}

				ltrd.add(trd);
			}
			return ltrd;
		}else
			throw new PolicyNotFoundException("Policy Not Found");

	}

	public EmployeePoliciesResponseDto enrollPolicy(EmployeePoliciesRequestDto requestDto) {
		EmployeePolicies empPolicy = new EmployeePolicies();
		empPolicy.setPolicyNum(requestDto.getPolicyNum());
		empPolicy.setPremiumAmount(requestDto.getPremiumAmount());
		Optional<Employee> emp = employeeRepository.findById(requestDto.getEmpId());
		empPolicy.setEmployee(emp.get());
		Optional<AvailablePolicies> policy = availablePoliciesRepository.findById(requestDto.getPolicyId());
		AvailablePolicies policies = policy.get();
		empPolicy.setAvailablePolicies(policies);
		empPolicy.setStartDate(policies.getStartDate());
		empPolicy.setEndDate(policies.getEndDate());
		EmployeePolicies empPolicyObj = employeePoliciesRepository.save(empPolicy);
		EmployeePoliciesResponseDto responseDto = new EmployeePoliciesResponseDto();
		responseDto.setMsg(AppConstants.ENROLLED);
		responseDto.setPolicyNum(empPolicyObj.getPolicyNum());
		return responseDto;
	}

}
