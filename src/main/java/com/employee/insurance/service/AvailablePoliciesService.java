package com.employee.insurance.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.employee.insurance.entity.AvailablePolicies;
import com.employee.insurance.exception.PolicyNotFoundException;
import com.employee.insurance.repository.AvailablePoliciesRepository;

@Service
public class AvailablePoliciesService {

	@Autowired
	AvailablePoliciesRepository availablePoliciesRepository;

	public List<AvailablePolicies> getPolicies() throws PolicyNotFoundException {

		List<AvailablePolicies> availablePolicies = availablePoliciesRepository.findAll();
		return availablePolicies;
	}

	public AvailablePolicies getPoliciesById(Long policyId) throws PolicyNotFoundException {
		Optional<AvailablePolicies> availablePolicies = availablePoliciesRepository.findByPolicyId(policyId);
		if (availablePolicies.isPresent()) {
			return availablePolicies.get();

		} else {
			throw new PolicyNotFoundException("Policy Not Found For This Product Code");
		}
	}

}
