package com.employee.insurance.dto;

public class EmployeePoliciesResponseDto {

	private String msg;
	private String policyNum;
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public String getPolicyNum() {
		return policyNum;
	}
	public void setPolicyNum(String policyNum) {
		this.policyNum = policyNum;
	}
	
	
	
}
