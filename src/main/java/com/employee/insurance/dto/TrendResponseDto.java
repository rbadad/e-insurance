package com.employee.insurance.dto;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder
public class TrendResponseDto {
	
	private String policyName;
	private String NoOfPolicyHolder;
	private String policy_percentage;
	
	public TrendResponseDto() {
		super();

	}
	
	public TrendResponseDto(String policyName, String noOfPolicyHolder, String policy_percentage) {
		super();
		this.policyName = policyName;
		NoOfPolicyHolder = noOfPolicyHolder;
		this.policy_percentage = policy_percentage;
	}

	public String getPolicyName() {
		return policyName;
	}
	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}
	public String getNoOfPolicyHolder() {
		return NoOfPolicyHolder;
	}
	public void setNoOfPolicyHolder(String noOfPolicyHolder) {
		NoOfPolicyHolder = noOfPolicyHolder;
	}

	public String getPolicy_percentage() {
		return policy_percentage;
	}

	public void setPolicy_percentage(String policy_percentage) {
		this.policy_percentage = policy_percentage;
	}
}
