package com.employee.insurance.dto;

public class EmployeePoliciesRequestDto {
	
	private double premiumAmount;
	private String policyNum;
	private Long empId;
	private Long policyId;
	
	
	public double getPremiumAmount() {
		return premiumAmount;
	}
	public void setPremiumAmount(double premiumAmount) {
		this.premiumAmount = premiumAmount;
	}
	public String getPolicyNum() {
		return policyNum;
	}
	public void setPolicyNum(String policyNum) {
		this.policyNum = policyNum;
	}
	public Long getEmpId() {
		return empId;
	}
	public void setEmpId(Long empId) {
		this.empId = empId;
	}
	public Long getPolicyId() {
		return policyId;
	}
	public void setPolicyId(Long policyId) {
		this.policyId = policyId;
	}
	public EmployeePoliciesRequestDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public EmployeePoliciesRequestDto(double premiumAmount, String policyNum, Long empId, Long policyId) {
		super();
		this.premiumAmount = premiumAmount;
		this.policyNum = policyNum;
		this.empId = empId;
		this.policyId = policyId;
	}
	@Override
	public String toString() {
		return "EmployeePoliciesRequestDto [premiumAmount=" + premiumAmount + ", policyNum=" + policyNum + ", empId="
				+ empId + ", policyId=" + policyId + "]";
	}
	
	
}
